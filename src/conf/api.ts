import axios from "axios";

declare module "axios" {
  export interface AxiosRequestConfig {
    authorization?: boolean;
  }
}

function createAxiosInstance(options: any) {
  const api = axios.create(options);
  api.interceptors.request.use(
    (config: any) => {
      return config;
    },
    (error) => {
      return Promise.reject(error);
    }
  );
  return api;
}

export const api = createAxiosInstance({
  url: import.meta.env.VITE_API_URL,
  baseURL: import.meta.env.VITE_API_URL,
  timeout: 30000,
  headers: {
    Authorization: `Bearer ${import.meta.env.VITE_API_TOKEN}`,
  },
});
