import React, { lazy } from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import {
  createBrowserRouter,
  createRoutesFromElements,
  Route,
  RouterProvider,
} from "react-router-dom";
import Root from "./routes/root";
import ErrorPage from "./error-page";
import "vite/modulepreload-polyfill";
import Loader from "./components/loader";
import { GraphQLClient, ClientContext } from "graphql-hooks";
import MentionsLegales from "./routes/MentionsLegales";
import ReactGA from "react-ga";

ReactGA.initialize("G-KR7HPNCS3H");
//import Home from "./routes/homePage/Home";
const ServicesPage = lazy(() => import("./routes/servicesPage"));
const Home = lazy(() => import("./routes/homePage/Home"));
const router = createBrowserRouter(
  createRoutesFromElements(
    <Route path="/" element={<Root />} errorElement={<ErrorPage />}>
      {/* <Route index element={<Home />} /> */}
      <Route
        path="/"
        element={
          <React.Suspense fallback={<Loader />}>
            <Home />
          </React.Suspense>
        }
      />
      <Route
        path="demande-thermostat"
        element={
          <React.Suspense fallback={<Loader />}>
            <ServicesPage />
          </React.Suspense>
        }
      />
      <Route
        path="mentions-legales"
        element={
          <React.Suspense fallback={<Loader />}>
            <MentionsLegales />
          </React.Suspense>
        }
      />
    </Route>
  )
);
const client = new GraphQLClient({
  url: import.meta.env.VITE_API_URL,
  //cache: new InMemoryCache(),
  headers: {
    Authorization: `Bearer ${import.meta.env.VITE_API_TOKEN} `,
  },
});

ReactDOM.createRoot(document.getElementById("root")!).render(
  <React.StrictMode>
    <ClientContext.Provider value={client}>
      <RouterProvider router={router} fallbackElement={<Loader />} />
    </ClientContext.Provider>
  </React.StrictMode>
);
