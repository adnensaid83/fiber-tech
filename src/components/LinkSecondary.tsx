import React from "react";
import { Link } from "react-router-dom";

export const LinkSecondary = ({
  children,
  path,
}: {
  children: React.ReactNode;
  path: string;
}) => {
  return (
    <Link to={path} className="group text-base font-semibold text-primary">
      <div className="group relative flex items-center gap-1 overflow-hidden rounded-full border-2 border-primary bg-primary px-24 py-3 text-primary duration-300 after:absolute after:bottom-0 after:left-0 after:right-0 after:top-0 after:z-0 after:bg-white group-hover:text-white group-hover:after:top-full after:motion-safe:duration-300">
        <span className="relative z-[1] motion-safe:duration-100">
          {children}
        </span>
      </div>
    </Link>
  );
};
