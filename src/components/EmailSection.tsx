import { Variants, motion } from "framer-motion";

export default function EmailSection() {
  const emailVariants: Variants = {
    offscreen: {
      y: 300,
      opacity: 0,
    },
    onscreen: {
      y: 0,
      opacity: 1,
      rotate: 0,
      transition: {
        type: "spring",
        duration: 0.4,
      },
    },
  };
  return (
    <div className="grid grid-cols-2 min-h-[500px] w-full">
      <div className="border-r-2">eden</div>
      <div className="bg-green"></div>
      <motion.div
        className={`flex-1`}
        initial="offscreen"
        whileInView="onscreen"
        viewport={{ once: true, amount: 1 }}
      >
        <motion.address
          className="not-italic text-3xl text-center leading-none font-medium md:text-5xl"
          variants={emailVariants}
        >
          <a href="mailto:web@adnensaid.fr">web@adnensaid.fr</a>
        </motion.address>
      </motion.div>
      <div className="flex-1">
        <p className="text-green200 text-5xl font-medium leading-none tracking-widest uppercase">
          Better
        </p>
        <p className="text-green200 text-5xl font-medium leading-none tracking-widest uppercase text-end">
          Call
        </p>
        <p className="text-orange text-[80px] tracking-widest leading-none">
          Adnen
        </p>
        <p className="text-4xl text-center">07 81 15 29 46</p>
      </div>
    </div>
  );
}
