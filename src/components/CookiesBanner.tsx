import { useState, useEffect } from "react";
import { getLocalStorage, setLocalStorage } from "../utils/storageHelper";
import { NavLink } from "react-router-dom";

export default function CookieBanner() {
  const [cookieConsent, setCookieConsent] = useState<boolean | null>(null);

  useEffect(() => {
    try {
      const storedCookieConsent = getLocalStorage("cookie_consent", null);
      setCookieConsent(storedCookieConsent);
    } catch (error) {
      console.error("Failed to access localStorage:", error);
    }
  }, [setCookieConsent]);
  useEffect(() => {
    if (cookieConsent !== null) {
      const newValue = cookieConsent ? "granted" : "denied";

      if (typeof window.gtag === "function") {
        window.gtag("consent", "update", {
          analytics_storage: newValue,
        });
      } else {
        console.warn("gtag function is not available.");
      }

      setLocalStorage("cookie_consent", cookieConsent);
    }
  }, [cookieConsent]);
  return (
    <div
      className={`fixed bottom-0 left-0 right-0 z-40
                        mx-auto my-10 ${
                          cookieConsent != null ? "hidden" : "flex"
                        } flex max-w-max 
                        flex-col items-center justify-between gap-4 rounded-lg bg-primary px-3 py-3 text-white shadow  
                         sm:flex-row md:max-w-screen-sm md:px-4`}
    >
      <div className="text-center">
        <NavLink to="/mentions-legales">
          <p className="text-base m-0">
            Nous utilisons des
            <span className="font-bold text-activeNav"> cookies</span> sur notre
            site.
          </p>
        </NavLink>
      </div>

      <div className="flex gap-2">
        <button
          className="rounded-lg border-2 px-4 py-2 text-white"
          onClick={() => setCookieConsent(false)}
        >
          Je refuse
        </button>
        <button
          className="rounded-lg bg-white px-4 py-2 text-primary"
          onClick={() => setCookieConsent(true)}
        >
          J'accepte
        </button>
      </div>
    </div>
  );
}
