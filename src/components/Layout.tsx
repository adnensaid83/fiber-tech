import { ReactNode } from "react";
import Breadcrump from "./Breadcrumbs";
import { Container } from "./Container";
import { Underline } from "./TitleSection";
import Markdown from "react-markdown";

export default function Layout({
  title,
  tag,
  children,
}: {
  title: string;
  tag?: string;
  children: ReactNode;
}) {
  return (
    <div className="fex-1">
      <div className="flex flex-col px-12 py-8 items-center bg-primary md:py-20 md:px-0">
        <p className="text-base font-light uppercase">{tag}</p>
        <h1 className="max-w-[800px] text-center text-white flex flex-col items-center prose prose-p:text-3xl prose-p:mb-4 prose-strong:text-orange">
          <Markdown>{title}</Markdown>
          <Underline />
        </h1>
      </div>
      <Container>
        <div className="px-12 lg:px-0">
          <Breadcrump />
          {children}
        </div>
      </Container>
    </div>
  );
}
