type Props = {
  r: number;
  g: number;
  b: number;
};

const CustomColor = ({ r, g, b }: Props) => {
  return (
    <style>{`
        :root {
          --color-primary: ${r} ${g} ${b};
        }
      `}</style>
  );
};

export default CustomColor;
