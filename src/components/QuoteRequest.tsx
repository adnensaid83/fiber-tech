import { useState } from "react";
import ArrowLeft from "../components/ArrowLeft";
import ArrowRight from "../components/ArrowRight";
import { Formiz, FormizStep, useForm } from "@formiz/core";
import { isEmail, isNumber } from "@formiz/validations";
import { FieldRadio } from "../components/RadioOption";
import { FieldInput } from "../components/FieldInput";
import { FieldCheckbox } from "../components/CheckboxOption";
import Spin from "../assets/Spin";
import { FieldTextarea } from "../components/FieldTextarea";
import { QuestionI } from "../interfaces/Service.interface";
import { LogLevel, buildClient } from "@datocms/cma-client-browser";
import { FieldPickIdenticalImages } from "../components/FieldPickIdenticalImages";
import { useNavigate } from "react-router-dom";
import X from "../assets/X";
import { Image } from "react-datocms";
const lastQuestion: QuestionI[] = [
  {
    name: "last",
    title: "Dernière étape:",
    choice: {
      name: "",
    },
    option: [
      {
        label: "Nom et Prénom",
        value: "fullname",
        order: "1",
      },
      {
        label: "Adresse mail",
        value: "email",
        order: "2",
      },
      {
        label: "Numéro de téléphone",
        value: "phone",
        order: "3",
      },
      {
        label: "Code postal",
        value: "postal",
        order: "4",
      },
      {
        label: "Présentez brièvement votre projet",
        value: "description",
        order: "5",
      },
    ],
  },
];
const captcha: QuestionI[] = [
  {
    name: "twoPictures",
    title: "Sélectionnez deux images identiques",
    choice: {
      name: "",
    },
    option: [
      {
        label: "",
        value: "https://www.datocms-assets.com/127745/1719926484-avion.avif",
        order: "1",
      },
      {
        label: "",
        value: "https://www.datocms-assets.com/127745/1719926491-chat.avif",
        order: "2",
      },
      {
        label: "",
        value: "https://www.datocms-assets.com/127745/1719926504-chien.avif",
        order: "3",
      },
      {
        label: "",
        value: "https://www.datocms-assets.com/127745/1719926509-fleur.jpg",
        order: "4",
      },
      {
        label: "",
        value: "https://www.datocms-assets.com/127745/1719926518-person.jpg",
        order: "5",
      },
      {
        label: "",
        value:
          "https://www.datocms-assets.com/127745/1719926527-reevolver.webp",
        order: "6",
      },
      {
        label: "",
        value: "https://www.datocms-assets.com/127745/1711923919-cee.png",
        order: "7",
      },
      {
        label: "",
        value:
          "https://www.datocms-assets.com/127745/1583844698-gift-habeshaw-pgxxjrtfnjq-unsplash-edited.png",
        order: "8",
      },
      {
        label: "",
        value:
          "https://www.datocms-assets.com/127745/1612503277-mayur-gala-2podhmrvlik-unsplash.jpg",
        order: "9",
      },
    ],
  },
];
const fakeDelay = (delay = 500) => new Promise((r) => setTimeout(r, delay));
type OptionDevis = {
  question: string;
  response: string[] | string;
};
type FormValues = any;
export default function QuoteRequest({
  service,
  image,
}: {
  service: any;
  image: any;
}) {
  const [status, setStatus] = useState("idle");
  const [showSuccessAlert, setShowSuccessAlert] = useState(false);
  const navigate = useNavigate();
  async function createDevisRecord({
    dataForm,
  }: {
    dataForm: {
      fullname: string;
      email: string;
      phone: string;
      postal: string;
      description: string;
      details: string;
    };
  }) {
    const client = buildClient({
      apiToken: import.meta.env.VITE_API_TOKEN,
      logLevel: LogLevel.BASIC,
    });
    try {
      await client.items.create({
        item_type: { type: "item_type", id: import.meta.env.VITE_REQUEST_ID },
        ...dataForm,
      });
      //console.log("Nouveau devis créé:", newDevis);
      setStatus("success");
      setShowSuccessAlert(true);
    } catch (error) {
      console.error('Erreur lors de la création du modèle "request" :', error);
    }
  }

  type AccI = {
    [key: string]: string | string[];
  };
  const getInitialValues = () => {
    const initialValues = service?.reduce((acc: AccI, question: QuestionI) => {
      if (question.choice.name === "unique") {
        acc[question.name] = "";
      } else if (question.choice.name === "multiple") {
        acc[question.name] = [];
      }
      return acc;
    }, {});
    return {
      ...initialValues,
      fullname: "",
      email: "",
      phone: "",
      postal: "",
      description: "",
    };
  };

  const combinedQuestions = service?.concat([...captcha, ...lastQuestion]);
  const handleSubmitStep = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    if (
      !form.currentStep ||
      !form.currentStep?.isValid ||
      !form.currentStep?.name
    ) {
      form.submitStep();
      return;
    }

    setStatus("loading");
    //console.log(`Submitting ${form.currentStep?.name}...`); // eslint-disable-line no-console
    await fakeDelay();
    setStatus("success");
    form.submitStep();
  };
  const form = useForm<FormValues>({
    ready: true,
    initialValues: getInitialValues(),
    onValidSubmit: async (values, form) => {
      let details: OptionDevis[] = [];

      const fieldNames = service?.map((i: QuestionI) => i.name);
      fieldNames?.forEach((fieldName: string) => {
        const value = values[fieldName as keyof FormValues];
        if (value) {
          const question = service?.filter((q: any) => q.name === fieldName)[0];
          if (question) {
            const title = question.title || "default";
            if (Array.isArray(value)) {
              details.push({
                question: title,
                response: [...value],
              });
            } else {
              details.push({
                question: title,
                response: value,
              });
            }
          }
        }
      });

      const dataForm = {
        fullname: values.fullname,
        email: values.email,
        phone: values.phone,
        postal: values.postal,
        description: values.description,
        details: JSON.stringify(details),
      };
      setStatus("loading");
      createDevisRecord({ dataForm });
      form.setErrors({
        name: "You can display an error after an API call",
      });
      const stepWithError = form.getStepByFieldName("name");
      if (stepWithError) {
        form.goToStep(stepWithError.name);
      }
    },
    //onValuesChange: () => console.log("values change"),
    //onValid: () => console.log("onValid"),
    //onInvalid: () => console.log("onInvalid"),
  });
  const isLoading = status === "loading" || form.isValidating;
  return (
    <>
      <div className="flex gap-12 py-8 lg:py-12 relative">
        <div className="flex-1">
          <Formiz connect={form}>
            <form noValidate onSubmit={handleSubmitStep} className="relative">
              {combinedQuestions?.map((question: any) => (
                <FormizStep
                  name={question.name}
                  key={question.name + question.title}
                >
                  {question.name === "last" ? (
                    <>
                      <div className="flex items-center gap-4 mb-12">
                        <span className="flex items-center justify-center text-xl border-2 border-primary rounded-full bg-primary text-white w-16 h-16 shrink-0">
                          {(form.currentStep?.index ?? 0) + 1}
                        </span>
                        <p className="mb-0 uppercase font-medium text-primary">
                          {question.title}
                        </p>
                      </div>
                      <div
                        className={`grid grid-cols-1 gap-8 mb-12 ${
                          length && length <= 2
                            ? "md:grid-cols-1"
                            : "md:grid-cols-2"
                        }`}
                      >
                        {question.option.map((o: any, i: number) =>
                          o.value === "email" ? (
                            <FieldInput
                              key={i}
                              name={o.value}
                              label={o.label}
                              required="Ce champ est obligatoire"
                              validations={[
                                {
                                  handler: isEmail(),
                                  message: "Not a valid email",
                                },
                              ]}
                            />
                          ) : o.value === "description" ? (
                            <FieldTextarea
                              key={i}
                              name={o.value}
                              label={o.label}
                              required="Ce champ est obligatoire"
                            />
                          ) : o.value === "fullname" ? (
                            <FieldInput
                              key={i}
                              name={o.value}
                              label={o.label}
                              required="Ce champ est obligatoire"
                              formatValue={(val) => (val || "").trim()}
                            />
                          ) : (
                            <FieldInput
                              key={i}
                              name={o.value}
                              label={o.label}
                              required="Ce champ est obligatoire"
                              formatValue={(val) => (val || "").trim()}
                              validations={[
                                {
                                  handler: isNumber(),
                                  message: "Not a valid input",
                                },
                              ]}
                            />
                          )
                        )}
                      </div>
                    </>
                  ) : question.name === "twoPictures" ? (
                    <>
                      <FieldPickIdenticalImages
                        name={question.name}
                        order={(form.currentStep?.index ?? 0) + 1}
                        label="SÉLECTIONNEZ DEUX IMAGES IDENTIQUES"
                        helper=""
                        options={question.option.map((q: any) => q.value)}
                      />
                    </>
                  ) : question.choice.name === "unique" ? (
                    <FieldRadio
                      name={question.name}
                      order={(form.currentStep?.index ?? 0) + 1}
                      label={question.title}
                      options={question.option}
                      required="Ce champ est obligatoire "
                    />
                  ) : (
                    <FieldCheckbox
                      name={question.name}
                      order={(form.currentStep?.index ?? 0) + 1}
                      label={question.title}
                      options={question.option}
                    />
                  )}
                </FormizStep>
              ))}
              {!!form.steps?.length && (
                <div className="grid grid-cols2 justify-between items-center">
                  {!form.isFirstStep && (
                    <button
                      className="col-start-1 w-[48px] h-[30px] drop-shadow-md border-[3px] border-primary rounded-md text-primary disabled:opacity-0 md:w-[50px] md:h-[35px]"
                      onClick={(e) => {
                        e.preventDefault();
                        form.goToPreviousStep();
                      }}
                    >
                      <ArrowLeft />
                    </button>
                  )}
                  {/*                 <div>
                  Step {(form.currentStep?.index ?? 0) + 1} /{" "}
                  {form.steps.length}
                </div> */}

                  {form.isLastStep ? (
                    <>
                      <button
                        type="submit"
                        disabled={
                          (form.isLastStep
                            ? !form.isValid
                            : !form.isStepValid) && form.isStepSubmitted
                        }
                        className={
                          "col-start-2 bg-primary text-white px-8 py-4 rounded-md text-l border-primary border-2 transition duration-300 md:text-xl md:px-20 md:py-4 md:hover:bg-transparent md:hover:text-primary"
                        }
                      >
                        {isLoading ? (
                          <div className="flex items-center gap-2">
                            <Spin />
                            Simuler la demande d'eligibilté
                          </div>
                        ) : (
                          <div>Simuler la demande d'eligibilté</div>
                        )}
                      </button>
                    </>
                  ) : (
                    <button
                      type="submit"
                      disabled={
                        (form.isLastStep ? !form.isValid : !form.isStepValid) &&
                        form.isStepSubmitted
                      }
                      className={
                        "col-start-2 w-[48px] h-[30px] flex justify-center items-center drop-shadow-md border-[3px] border-primary rounded-md text-primary disabled:opacity-0 md:w-[50px] md:h-[35px]"
                      }
                    >
                      {!isLoading ? <ArrowRight /> : <Spin />}
                    </button>
                  )}
                </div>
              )}
            </form>
          </Formiz>
        </div>
        <div className="hidden lg:block">
          <Image data={image} />
        </div>
      </div>
      {showSuccessAlert && (
        <div className="fixed w-full h-full top-0 left-0 bg-primary  flex items-center justify-center">
          <div
            className="relative bg-white text-white rounded-xl m-4"
            role="alert"
          >
            <p className="m-0 text-2xl px-28 py-12 text-center text-primary md:text-3xl">
              Félicitation! Vous êtes éligible et le devis est gratuit. Vous
              serez contacté dans les 48 heures
            </p>
            <span
              className="block w-16 absolute top-0 right-0 cursor-pointer text-primary"
              onClick={() => {
                navigate("/");
                setShowSuccessAlert(false);
              }}
            >
              <X />
            </span>
          </div>
        </div>
      )}
    </>
  );
}
