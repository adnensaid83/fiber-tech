import { useQuery } from "graphql-hooks";
import Loader from "./loader";
import { Container } from "./Container";
import { NavLink } from "react-router-dom";

export function Footer() {
  const { loading, error, data } = useQuery(FOOTER_QUERY);
  if (loading) return <Loader />;
  if (error) return "error";
  return (
    <div className="py-12 bg-gray md:py-24">
      <Container>
        <div className="flex flex-col items-center gap-0 mb-24">
          <div className="w-[50px]">
            {/*  <Image data={data.layout.footerLogo.responsiveImage} /> */}
          </div>
          <p className="text-base font-medium mb-0">
            {data.layout.footerSubtitle}
          </p>
          <NavLink to={data.legalPage.slug} className={`text-base font-medium`}>
            {data.legalPage.title}
          </NavLink>
        </div>
        <div>
          <p className="text-[10px] text-center font-medium m-0 line-clamp-3 md:text-sm">
            Conçu et réalisé par
            <NavLink to={"https://adnensaid.fr/"} className="text-primary">
              Adnen Said
            </NavLink>
          </p>
        </div>
      </Container>
    </div>
  );
}
const FOOTER_QUERY = `
  query {
layout {
    footerLogo {
      responsiveImage(imgixParams: {}) {
        srcSet
        webpSrcSet
        sizes
        src
        height
        aspectRatio
        alt
        title
        base64
      }
    }
    footerSubtitle
  }
  legalPage {
    slug
    title
  }
  }
`;
