export function CircularText({ text }: { text: string }) {
  return (
    <svg viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
      <path
        fill="none"
        strokeWidth="2"
        stroke="none"
        id="circlePath"
        d="
      M 10, 50
      a 40,40 0 1,1 80,0
      40,40 0 1,1 -80,0
    "
      />
      <text
        id="text"
        fontFamily="monospace"
        fontSize="14"
        fontWeight="bold"
        fill="#1d1d1d"
      >
        <textPath
          id="textPath"
          href="#circlePath"
          className="uppercase tracking-widest"
        >
          {text}
        </textPath>
      </text>
    </svg>
  );
}

/* 

    <svg viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
      <path
        id="circlePath"
        fill="none"
        strokeWidth="4"
        stroke="hsl(0 100% 50% / 0.5)"
        d="
          M 10, 50
          a 40,40 0 1,1 80,0
          a 40,40 0 1,1 -80,0
        "
      />
      <text
        id="text"
        fontFamily="monospace"
        fontSize="12"
        fontWeight="bold"
        fill="green"
      >
        <textPath id="textPath" href="#circlePath"></textPath>
      </text>
    </svg>
*/
