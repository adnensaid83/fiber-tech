import { useQuery } from "graphql-hooks";
import Layout from "../components/Layout";
import { Underline } from "../components/TitleSection";
import Loader from "../components/loader";
import QuoteRequest from "../components/QuoteRequest";

export default function ServicesPage() {
  const { loading, error, data } = useQuery(SERVICES_QUERY);
  if (loading) return <Loader />;
  if (error) return "error";
  const { info, label, thermostatImage, sections } = data.allPages[0];
  return (
    <Layout title={info}>
      <h2 className="flex flex-col gap-6 pb-12 text-3xl lg:text-4xl lg:text-start lg:items-start">
        {label}
        <Underline />
      </h2>
      <QuoteRequest
        service={sections}
        image={thermostatImage.responsiveImage}
      />
    </Layout>
  );
}

const SERVICES_QUERY = `
query MyQuery {
  allPages(filter: {slug: {eq: "demande-thermostat"}}) {
    id
    label
    info
    slug
    thermostatImage {
      responsiveImage(imgixParams: {}) {
        srcSet
        webpSrcSet
        sizes
        src
        width
        height
        aspectRatio
        alt
        title
        base64
      }
    }
    sections {
      ... on QuestionRecord {
        id
        name
        order
        title
        choice {
          name
        }
        option {
          id
          label
          order
          title
          value
        }
      }
    }
  }
}
`;

/* const SERVICES_QUERY = gql`
  query GetServicePage {
    servicePage {
      id
      name
      slug
      bigTitle
      smallTitle
      description
      services {
        id
        order
        slug
        title
        description
        tag {
          title
        }
      }
    }
  }
`; */

/* 

export default function ServicesPage() {
  const { loading, error, data } = useQuery(SERVICES_QUERY, {
    variables: { limit: 4 },
  });

  if (loading) return <Loader />;
  if (error) return "error";
  return (
    <Layout title={data.servicePage.bigTitle}>
      <h2 className="flex flex-col gap-6 pb-12 text-3xl lg:text-4xl lg:text-start lg:items-start">
        {data.servicePage.smallTitle}
        <Underline />
      </h2>
      <div className="max-w-[800px] mb-12 prose prose-xl prose-p:text-dark prose-p:text-xl prose-p:leading-normal">
        <Markdown>{data.servicePage.description}</Markdown>
      </div>
      <ul
        className={`grid grid-cols-1 border-b-[3px] border-dark transition ease-out duration-400 md:grid-cols-1 md:group/item lg:grid-cols-2`}
      >
        <RegularList
          items={data.servicePage.services}
          resourceName="service"
          itemComponent={NavItem}
        />
      </ul>
    </Layout>
  );
}

const SERVICES_QUERY = gql`
  query GetServicePage {
    servicePage {
      id
      name
      slug
      bigTitle
      smallTitle
      description
      services {
        id
        order
        slug
        title
        description
        tag {
          title
        }
      }
    }
  }
`;
*/
