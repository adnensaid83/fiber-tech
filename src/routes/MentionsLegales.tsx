import { useQuery } from "graphql-hooks";
import Loader from "../components/loader";
import Layout from "../components/Layout";
import Markdown from "react-markdown";

const MentionsLegales = () => {
  const { loading, error, data } = useQuery(MENTIONSLEGALES_QUERY);
  if (loading) return <Loader />;
  if (error) return "error";
  return (
    <Layout title="Mentions legales">
      <div>
        <Markdown>{data.legalPage.content}</Markdown>
      </div>
    </Layout>
  );
};

export default MentionsLegales;

const MENTIONSLEGALES_QUERY = `
  query {
  legalPage {
    content
    slug
    title
  }
  }
`;
