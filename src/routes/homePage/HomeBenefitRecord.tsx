import { Image } from "react-datocms";
import { Container } from "../../components/Container";
import { LinkPrimary } from "../../components/LinkPrimary";
import { Underline } from "../../components/TitleSection";
import Section from "../../components/styles/Section";
import { CoverImageI } from "../../interfaces/Project.interface";
import Markdown from "react-markdown";
export interface BenefitRecordI {
  id: string;
  target: string;
  __typename: string;
  benefitTitle: string;
  buttonBenefit: {
    label: string;
    primaryButton: boolean;
    url: string;
  };
  benefit: {
    benefitImage: CoverImageI;
    benefitName: string;
    benefitContent: string;
  }[];
}
export const BenefitRecord = ({
  details,
  bg,
}: {
  details: BenefitRecordI;
  bg: string;
}) => {
  const { target, benefitTitle, buttonBenefit, benefit } = details;
  return (
    <Section id={target} bg={bg}>
      <h2 className="prose prose-strong:text-primary prose-strong:font-medium flex flex-col items-center justify-center gap-2 prose-p:m-0 prose-p:text-3xl prose-p:text-black font-semibold text-center mb-16 uppercase md:mb-24 md:gap-8">
        <Markdown>{benefitTitle}</Markdown>
        <Underline />
      </h2>
      <Container>
        <div className="flex flex-col gap-12">
          <ul className="flex flex-col gap-16 lg:flex-row lg:justify-between">
            {benefit.map((b, index) => (
              <li key={index} className="flex flex-col items-center">
                <div className="w-16 h-20">
                  <Image data={b.benefitImage?.responsiveImage} />
                </div>
                <h3 className="text-center text-2xl"> {b.benefitName} </h3>
                <div className="text-center text-xl prose prose-p:text-xl prose-strong:font-normal prose-p:text-black prose-strong:text-primary">
                  <Markdown>{b.benefitContent}</Markdown>
                </div>
              </li>
            ))}
          </ul>
          <div className="text-xl flex justify-center">
            <LinkPrimary path={buttonBenefit.url}>
              {buttonBenefit.label}
            </LinkPrimary>
          </div>
        </div>
      </Container>
      {/*       <Container>
        <div>
          <ul className="grid grid-cols-1 place-items-center gap-6 mb-16 md:grid-cols-2 md:gap-16 md:mb-24 lg:grid-cols-4 lg:gap-16 ">
            <RegularList
              items={projectDetails}
              resourceName="project"
              itemComponent={ProjectListItem}
            />
          </ul>
          <div className="text-center">
            <NavLink
              to={path}
              className="inline-block bg-orange text-white px-16 py-4 rounded-md text-l border-orange border-2 transition duration-300 md:text-xl md:px-20 md:py-4 md:hover:bg-transparent md:hover:text-orange"
            >
              {buttontext}
            </NavLink>
          </div>
        </div>
      </Container> */}
    </Section>
  );
};
