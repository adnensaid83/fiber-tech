import GeoIcon from "../../assets/GeoIcon";
import MailIcon from "../../assets/MailIcon";
import PhoneIcon from "../../assets/phoneIcon";
import { Underline } from "../../components/TitleSection";
import Section from "../../components/styles/Section";

export interface HomeContactRecordI {
  id: string;
  __typename: string;
  bigTitle: string;
  smallTitle: string;
  emailLabel: string;
  email: string;
  phoneLabel: string;
  phone: string;
  adresseLabel: string;
  adresse: string;
}
export const HomeContactRecord = ({
  details,
  bg,
}: {
  details: HomeContactRecordI;
  bg: string;
}) => {
  const {
    bigTitle,
    smallTitle,
    emailLabel,
    email,
    phoneLabel,
    phone,
    adresseLabel,
    adresse,
  } = details;
  return (
    <Section id="contact" bg={bg}>
      <h2 className="flex flex-col items-center justify-center gap-2 text-3xl font-semibold text-center mb-16 md:mb-24 md:text-4.4xl md:gap-8">
        {bigTitle}
        <Underline />
      </h2>
      <p className="text-2xl text-center md:text-3xl lg:text-4xl">
        {smallTitle}
      </p>
      <div className="text-4xl mb-12 md:text-5xl md:mb-16">👇</div>
      <div className="flex flex-col gap-12  md:mb-16 md:flex-row lg:gap-44">
        <ContactItem icon={<MailIcon />} label={emailLabel} value={email} />
        <ContactItem icon={<PhoneIcon />} label={phoneLabel} value={phone} />
        <ContactItem icon={<GeoIcon />} label={adresseLabel} value={adresse} />
      </div>
    </Section>
  );
};
type ContactItemProps = {
  icon: any;
  label: string;
  value: string;
};
function ContactItem({ icon, label, value }: ContactItemProps) {
  return (
    <div className="flex gap-4">
      <div className="w-[48px] h-[48px] md:w-[60px] md:h-[60px] p-5 rounded-full bg-white drop-shadow-md text-orange border-2 border-white">
        {icon}
      </div>
      <div>
        <h3 className="text-l md:text-xl font-bold"> {label} </h3>
        <address className="not-italic text-l text-center leading-none font-light text-gray100 md:text-xl">
          <a href="mailto:web@adnensaid.fr"> {value} </a>
        </address>
      </div>
    </div>
  );
}
