import { Image } from "react-datocms";
import { Underline } from "../../components/TitleSection";
import Section from "../../components/styles/Section";
import { CoverImageI } from "../../interfaces/Project.interface";
import Markdown from "react-markdown";

export interface ModernRecordI {
  id: string;
  __typename: string;
  target: string;
  modernTitle: string;
  modernContent: string;
  modernImage: CoverImageI;
}
export const ModernRecord = ({ details }: { details: ModernRecordI }) => {
  const { target, modernTitle, modernContent, modernImage } = details;
  return (
    <Section id={target} bg="gray">
      <div className="max-w-[850px] mx-auto">
        <div className="flex gap-12 flex-col md:flex-row md:items-center">
          <div className="flex-1">
            <h2 className="flex flex-col gap-2 font-semibold uppercase mb-12 md:gap-8">
              <Markdown className="prose prose-strong:text-primary prose-strong:font-semibold prose-p:text-black prose-p:m-0 prose-p:text-3xl">
                {modernTitle}
              </Markdown>
              <Underline />
            </h2>
            <Markdown className="porse prose-strong:text-primary prose-strong:font-normal">
              {modernContent}
            </Markdown>
          </div>
          <div className="flex-1">
            <Image data={modernImage.responsiveImage} />
          </div>
        </div>
      </div>
    </Section>
  );
};
