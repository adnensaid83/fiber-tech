import { NavLink } from "react-router-dom";
import { Container } from "../../components/Container";
import Section from "../../components/styles/Section";
import { CoverImageI } from "../../interfaces/Project.interface";
import { Image } from "react-datocms";
export interface BrandsRecordI {
  id: string;
  __typename: string;
  target: string;
  brandTitle: string;
  brand: {
    id: string;
    brandLogo: CoverImageI;
    brandName: string;
    brandUrl: string;
  }[];
}
export const BrandsRecord = ({ details }: { details: BrandsRecordI }) => {
  const { target, brand } = details;
  return (
    <Section id={target} bg="white">
      {/*       <h2 className="flex flex-col items-center justify-center gap-2 prose prose-p:text-4xl prose-p:my-0 prose-p:text-black prose-strong:text-primary font-semibold text-center mb-16 md:mb-24 md:text-4.4xl md:gap-8">
        <Markdown>{brandTitle}</Markdown>
        <Underline />
      </h2> */}
      <Container>
        <div className="flex flex-col gap-8 lg:flex-row">
          {brand.map((b, i) => (
            <NavLink to={b.brandUrl} key={i} target="_blank" className="flex-1">
              <div className="flex gap-2 items-center p-12 min-h-[200px] rounded-xl bg-gray border-2 border-transparent hover:border-gray hover:bg-white">
                <Image
                  data={b.brandLogo.responsiveImage}
                  className="max-w-[6px]"
                />
                <p>{b.brandName}</p>
              </div>
            </NavLink>
          ))}
        </div>
      </Container>
    </Section>
  );
};
