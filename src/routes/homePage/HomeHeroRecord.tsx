import { SplitScreen } from "../../components/SplitScreen";
import { CoverImageI } from "../../interfaces/Project.interface";
import { Image } from "react-datocms";
import { LinkPrimary } from "../../components/LinkPrimary";
import Markdown from "react-markdown";

// Hero section
export type HeroRecordI = {
  id: string;
  __typename: string;
  target: string;
  heroTitle?: string;
  heroSubtitle?: string;
  buttonHero: {
    label: string;
    primaryButton: boolean;
    url: string;
  };
  heroImage: CoverImageI;
};
export const HeroRecord = ({ details }: { details: HeroRecordI }) => {
  const { target, heroTitle, heroSubtitle, heroImage, buttonHero } = details;
  return (
    <div id={target} className="py-16 px-12 md:py-28">
      <div className="max-w-[993px] mx-auto">
        <SplitScreen
          className="flex items-center flex-col-reverse gap-8 lg:flex-row lg:justify-between lg:gap-32"
          leftStyle="max-w-[500px]"
          rightStyle="relative"
        >
          <BannerLeft
            title={heroTitle ? heroTitle : ""}
            content={heroSubtitle ? heroSubtitle : ""}
            buttonText={buttonHero.label ? buttonHero.label : ""}
            path={buttonHero.url}
          />
          <BannerRight image={heroImage} />
        </SplitScreen>
      </div>
    </div>
  );
};

function BannerRight({ image }: { image: CoverImageI }) {
  return (
    <div className="border-[4px] rounded-full overflow-hidden flex morphAnimation border-primary w-[325px] h-[325px] md:w-[450px] md:h-[450px]">
      <Image data={image.responsiveImage} />
    </div>
  );
}

interface BannerLeftProps {
  title: string;
  content: string;
  buttonText: string;
  path: string;
}
function BannerLeft({ title, content, buttonText, path }: BannerLeftProps) {
  return (
    <>
      <h2 className="p-0 pb-8 text-center prose-strong:text-primary prose-strong:font-normal prose-p:text-3xl prose-p:my-0 lg:pb-12 lg:prose-p:text-5xl lg:text-start">
        <Markdown>{title}</Markdown>
      </h2>
      <div className="pb-8 prose-strong:text-primary prose-strong:font-normal prose-p:my-0 prose-p:text-xl word-spacing-l text-center lg:pb-12 line-clamp-4 lg:text-start">
        <Markdown>{content}</Markdown>
      </div>
      <div className="flex justify-center text-xl lg:justify-start">
        <LinkPrimary path={path}>{buttonText}</LinkPrimary>
      </div>
    </>
  );
}
