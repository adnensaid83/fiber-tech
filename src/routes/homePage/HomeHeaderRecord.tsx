import { ReactNode, useRef } from "react";
import { CoverImageI } from "../../interfaces/Project.interface";
import { Image } from "react-datocms";
import { List, X } from "../../assets";
import onPress from "../../utils/scrollFunction";
import { Link } from "react-router-dom";
import { LinkSecondary } from "../../components/LinkSecondary";

const HomeHeaderRecord = ({
  logo,
  sections,
}: {
  logo: CoverImageI;
  sections: string[];
}) => {
  const collapseNavbarMenuRef = useRef<HTMLDivElement | null>(null);
  const handleCollapseNavbarMenu = () => {
    const collapse = collapseNavbarMenuRef.current;
    collapse?.classList.toggle("hidden");
  };
  return (
    <header className="sticky top-0 z-10 shadow-nav backdrop-blur bg-header">
      <div className="max-w-[1200px] mx-auto">
        <nav className="flex items-center justify-between py-4 px-12 lg:justify-start lg:py-6 md:gap-20">
          <div className="flex items-center gap-2">
            <div className="w-[50px]">
              <Image data={logo.responsiveImage} />
            </div>
            <span className="uppercase text-base text-primary font-extrabold leading-none">
              Fiber tech <br /> energy
            </span>
          </div>
          <div className="flex lg:hidden">
            <NavbarToggler
              imgCompnent={<List />}
              onCollapseNavbar={handleCollapseNavbarMenu}
              className={` w-[36px] h-[36px] p-0 focus:outline-0`}
            />
          </div>
          <div className="hidden lg:flex-1 lg:flex">
            <div className="relative flex-1 flex items-center justify-between gap-6 font-light text-[15px]">
              <ul className="flex">
                {sections.map((section, index) => (
                  <li
                    key={index}
                    className={`mt-0 font-semibold text-2xl hover:opacity-80 py-6 md:text-xl lg:px-8 lg:py-2 lg:text-base`}
                    data-to-scrollspy-id={section}
                  >
                    <Link
                      to={`#${section}`}
                      onClick={(e) => {
                        handleCollapseNavbarMenu();
                        onPress(e);
                      }}
                    >
                      {section}
                    </Link>
                  </li>
                ))}
              </ul>
              <LinkSecondary path={"/demande-thermostat"}>
                Contact
              </LinkSecondary>
            </div>
          </div>
        </nav>
        <div className="hidden lg:hidden" ref={collapseNavbarMenuRef}>
          <div
            className={`fixed min-h-screen inset-y-0 right-0 left-0 top-0 bottom-0 z-40 w-full flex flex-col overflow-y-auto bg-white`}
          >
            <div className="flex justify-between items-center py-4 px-12 lg:py-6">
              <div className="flex items-center gap-2">
                <div className="w-[50px]">
                  <Image data={logo.responsiveImage} />
                </div>
                <span className="uppercase text-base text-primary font-extrabold leading-none">
                  Fiber tech <br /> energy
                </span>
              </div>
              <div className="flex lg:hidden">
                <NavbarToggler
                  imgCompnent={<X />}
                  onCollapseNavbar={handleCollapseNavbarMenu}
                  className={` w-[36px] h-[36px] p-0 focus:outline-0 border-2 border-grayModal`}
                />
              </div>
            </div>
            <div className="flex-1 flex flex-col gap-12 bg-white">
              <ul className="list-none font-light text-4xl space-y-0 p-12">
                {sections.map((section, index) => (
                  <li
                    key={index}
                    className={`mt-0 text-black font-semibold hover:opacity-80 py-6 lg:px-8 lg:py-2`}
                    data-to-scrollspy-id={section}
                  >
                    <Link
                      to={`#${section}`}
                      onClick={(e) => {
                        handleCollapseNavbarMenu();
                        onPress(e);
                      }}
                    >
                      {section}
                    </Link>
                  </li>
                ))}
                <Link
                  to={"/demande-thermostat"}
                  className="block font-semibold text-4xl py-6"
                >
                  Contact
                </Link>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </header>
  );
};

export default HomeHeaderRecord;

type NavbarToggler = {
  imgCompnent: ReactNode;
  onCollapseNavbar: () => void;
  className: string;
};
function NavbarToggler({
  imgCompnent,
  onCollapseNavbar,
  className,
}: NavbarToggler) {
  return (
    <button className={className} type="button" onClick={onCollapseNavbar}>
      {imgCompnent}
    </button>
  );
}
