import Loader from "../../components/loader";
import { HeroRecordI } from "./HomeHeroRecord";
import HomeSection from "./HomeSection";
import ScrollSpy from "react-ui-scrollspy";
import { BrandsRecordI } from "./HomeBrandsRecord";
import { ModernRecordI } from "./HomeModernRecord";
import { HomeTestimonialRecordI } from "./HomeTestimonialRecord";
import { BenefitRecordI } from "./HomeBenefitRecord";
import { HomeContactRecordI } from "./HomeContactRecord";
import HomeHeaderRecord from "./HomeHeaderRecord";
import { useQuery } from "graphql-hooks";
import { ParticipateRecordI } from "./HomeParticipateRecord";

export type SectionI =
  | HeroRecordI
  | BrandsRecordI
  | BenefitRecordI
  | ModernRecordI
  | ParticipateRecordI
  | HomeTestimonialRecordI
  | HomeContactRecordI;

const Home = () => {
  const { loading, error, data } = useQuery(HOME_QUERY);
  if (loading) return <Loader />;
  if (error) return "error";
  return (
    <div>
      <HomeHeaderRecord
        logo={data.layout.logo}
        sections={data.allPages[0].sections.map((s: any) => s.target)}
      />
      <ScrollSpy scrollThrottle={30} useBoxMethod={false}>
        {data.allPages[0].sections.map((section: SectionI) => (
          <HomeSection key={section.id} details={section} />
        ))}
      </ScrollSpy>
    </div>
  );
};

export default Home;

const HOME_QUERY = `
  query GetHomePage {
    allPages(filter: { slug: { eq: "home" } }) {
      label
      slug
      sections {
        ... on HeroSectionRecord {
          id
          __typename
          target
          heroTitle
          heroSubtitle
          heroImage {
            responsiveImage(imgixParams: {}) {
              srcSet
              webpSrcSet
              sizes
              src
              height
              aspectRatio
              alt
              title
              base64
            }
          }
          buttonHero {
            primaryButton
            url
            label
          }
        }
        ... on BrandsSectionRecord {
          id
          __typename
          target
          brandTitle
          brand {
            id
            brandUrl
            brandName
            brandLogo {
              responsiveImage(imgixParams: {fit: clip, w: "60", h: "60", auto: format}) {
                srcSet
                webpSrcSet
                sizes
                src
                width
                height
                aspectRatio
                alt
                title
                base64
              }
            }
          }
        }
        ... on BenefitSectionRecord {
          id
          __typename
          target
          benefitTitle
          buttonBenefit {
            label
            primaryButton
            url
          }
          benefit {
            benefitImage {
              responsiveImage(imgixParams: {fit:max, q: 60, w: 50, h: 50}) {
                srcSet
                webpSrcSet
                sizes
                src
                height
                aspectRatio
                alt
                title
                base64
              }
            }
            benefitName
            benefitContent
          }
        }
        ... on ModernRecord {
          id
          __typename
          target
          modernTitle
          modernContent
          modernImage {
            responsiveImage(imgixParams: {}) {
              srcSet
              webpSrcSet
              sizes
              src
              height
              aspectRatio
              alt
              title
              base64
            }
          }
        }
        ... on ParticipateSectionRecord {
          id
          __typename
          target
          participateTitle
          participateContent
          buttonParticipate {
            label
            primaryButton
            url
          }
        }
      }
    }
    layout {
      logo {
        responsiveImage(imgixParams: {}) {
          srcSet
          webpSrcSet
          sizes
          src
          width
          height
          aspectRatio
          alt
          title
        }
      }
      accentColor {
        blue
        green
        red
      }
    }
  }
`;
