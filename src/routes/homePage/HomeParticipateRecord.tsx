import { Container } from "../../components/Container";
import { LinkPrimary } from "../../components/LinkPrimary";
import { Underline } from "../../components/TitleSection";
import Section from "../../components/styles/Section";
import Markdown from "react-markdown";

export interface ParticipateRecordI {
  id: string;
  __typename: string;
  target: string;
  participateTitle: string;
  participateContent: string;
  buttonParticipate: {
    label: string;
    primaryButton: boolean;
    url: string;
  };
}

const HomeParticipateRecord = ({
  details,
}: {
  details: ParticipateRecordI;
}) => {
  const { target, participateTitle, participateContent, buttonParticipate } =
    details;
  return (
    <Section id={target} bg="white">
      <h2 className="prose prose-strong:text-primary prose-strong:font-semibold flex flex-col items-center justify-center gap-2 prose-p:m-0 prose-p:text-3xl prose-p:text-black font-semibold text-center mb-16 uppercase md:mb-24 md:gap-8">
        <Markdown>{participateTitle}</Markdown>
        <Underline />
      </h2>
      <Container>
        <div className="flex flex-col items-center gap-12">
          <Markdown className="prose prose-p:text-center prose-p:text-black prose-strong:font-normal prose-strong:text-primary">
            {participateContent}
          </Markdown>
          <div>
            <LinkPrimary path={buttonParticipate.url}>
              {buttonParticipate.label}
            </LinkPrimary>
          </div>
        </div>
      </Container>
    </Section>
  );
};

export default HomeParticipateRecord;
