import { SectionI } from "./Home";
import { BrandsRecord, BrandsRecordI } from "./HomeBrandsRecord";
import { ModernRecord, ModernRecordI } from "./HomeModernRecord";
import {
  HomeTestimonialRecord,
  HomeTestimonialRecordI,
} from "./HomeTestimonialRecord";
import { BenefitRecord, BenefitRecordI } from "./HomeBenefitRecord";
import { HomeContactRecord, HomeContactRecordI } from "./HomeContactRecord";
import { HeroRecord, HeroRecordI } from "./HomeHeroRecord";
import HomeParticipateRecord, {
  ParticipateRecordI,
} from "./HomeParticipateRecord";

const isHomeHeroRecord = (details: SectionI): details is HeroRecordI => {
  return details.__typename === "HeroSectionRecord";
};

const isHomeBrandsRecord = (details: SectionI): details is BrandsRecordI => {
  return details.__typename === "BrandsSectionRecord";
};

const isHomeModernRecord = (details: SectionI): details is ModernRecordI => {
  return details.__typename === "ModernRecord";
};

const isHomeTestimonialRecord = (
  details: SectionI
): details is HomeTestimonialRecordI => {
  return details.__typename === "HomeTestimonialRecord";
};

const isHomeBenefitRecord = (details: SectionI): details is BenefitRecordI => {
  return details.__typename === "BenefitSectionRecord";
};
const isHomeParticipateRecord = (
  details: SectionI
): details is ParticipateRecordI => {
  return details.__typename === "ParticipateSectionRecord";
};
const isHomeContactRecord = (
  details: SectionI
): details is HomeContactRecordI => {
  return details.__typename === "HomeContactRecord";
};

const HomeSection = ({ details }: { details: SectionI }) => {
  if (isHomeHeroRecord(details)) {
    return <HeroRecord details={details} />;
  }
  if (isHomeBrandsRecord(details)) {
    return <BrandsRecord details={details} />;
  }
  if (isHomeModernRecord(details)) {
    return <ModernRecord details={details} />;
  }
  if (isHomeTestimonialRecord(details)) {
    return <HomeTestimonialRecord details={details} />;
  }
  if (isHomeBenefitRecord(details)) {
    return <BenefitRecord details={details} bg="white" />;
  }
  if (isHomeContactRecord(details)) {
    return <HomeContactRecord details={details} bg="green" />;
  }
  if (isHomeParticipateRecord(details)) {
    return <HomeParticipateRecord details={details} />;
  }
  return <></>;
};

export default HomeSection;
