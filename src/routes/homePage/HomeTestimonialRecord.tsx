import { Image } from "react-datocms";
import QuoteIcon from "../../assets/QuoteIcon";
import { TestimonialI } from "../../interfaces/Testimonial.interface";
import ArrowLeft from "../../components/ArrowLeft";
import ArrowRight from "../../components/ArrowRight";
import Section from "../../components/styles/Section";
import { Underline } from "../../components/TitleSection";
import { Container } from "../../components/Container";
import { CoverImageI } from "../../interfaces/Project.interface";
import { useState } from "react";
import { Navigation, Pagination, Virtual } from "swiper/modules";
import { Swiper, SwiperSlide, useSwiper } from "swiper/react";
import { NavLink } from "react-router-dom";
export interface HomeTestimonialRecordI {
  id: string;
  __typename: string;
  bigTitle: string;
  comment: TestimonialI[];
}

export const HomeTestimonialRecord = ({
  details,
}: {
  details: HomeTestimonialRecordI;
}) => {
  const [currentIndex, setCurrentIndex] = useState(0);
  const { bigTitle, comment } = details;
  return (
    <Section id="temoignages" bg="white">
      <h2 className="flex flex-col items-center justify-center gap-2 text-3xl font-semibold text-center mb-16 uppercase md:mb-24 md:text-4.4xl md:gap-8">
        {bigTitle}
        <Underline />
      </h2>
      <Container>
        <div className="text-end underline text-l">
          <NavLink to={"ajout-commentaire"}>Ajouter un avis</NavLink>
        </div>
        <Swiper
          modules={[Navigation, Pagination, Virtual]}
          spaceBetween={50}
          slidesPerView={1}
          navigation
          pagination={{ clickable: true }}
          onSlideChange={(swiper) => setCurrentIndex(swiper.activeIndex)}
          onSwiper={(swiper) => setCurrentIndex(swiper.activeIndex)}
          virtual
        >
          {comment.map((t: TestimonialI, index: number) => {
            return (
              <SwiperSlide key={index} virtualIndex={index}>
                <TestimonialsItem testimonial={t} />
              </SwiperSlide>
            );
          })}
          <div className="hidden md:flex md:justify-center md:gap-16 md:pb-2 md:pt-32">
            <SlidePrevButton currentIndex={currentIndex} />
            <SlideNextButton
              currentIndex={currentIndex}
              testimonials={comment}
            />
          </div>
        </Swiper>
      </Container>
    </Section>
  );
};
function SlideNextButton({
  currentIndex,
  testimonials,
}: {
  currentIndex: number;
  testimonials: TestimonialI[];
}) {
  const swiper = useSwiper();
  return (
    <button
      className="w-[48px] h-[48px] p-5 rounded-full bg-white drop-shadow-md border-2 border-white text-orange disabled:text-gray md:w-[60px] md:h-[60px]"
      disabled={currentIndex === testimonials.length - 1}
      onClick={() => swiper.slideNext()}
    >
      <ArrowRight />
    </button>
  );
}
function SlidePrevButton({ currentIndex }: { currentIndex: number }) {
  const swiper = useSwiper();
  return (
    <button
      className={`w-[48px] h-[48px] p-5 rounded-full bg-white drop-shadow-md border-2 border-white text-orange disabled:text-gray q md:w-[60px] md:h-[60px]`}
      disabled={currentIndex === 0}
      onClick={() => {
        swiper.slidePrev();
      }}
    >
      <ArrowLeft />
    </button>
  );
}
export function TestimonialsItem({
  testimonial,
}: {
  testimonial: TestimonialI;
}) {
  const { fullname, img, profession, description } = testimonial;

  return (
    <li className="group flex flex-col lg:px-20">
      <div className="">
        <div className="text-orange max-w-[80px] m-auto md:max-w-[100px]">
          <QuoteIcon />
        </div>
        <p className="mb-8 font-light text-2xl text-center text-slate-500 dark:text-dark md:text-3xl">
          {description}
        </p>
      </div>
      <div className="flex flex-col items-center gap-2 md:flex-row md:justify-center">
        <ImgProfil img={img} />
        <div className="flex flex-col items-center divide-orange md:flex-row md:divide-x-2">
          <p className="font-medium text-slate-300 mb-0 pr-4 group-hover:text-dark">
            {fullname}
          </p>
          <p className="font-light text-orange mb-0 pl-4 group-hover:text-orange">
            {profession}
          </p>
        </div>
      </div>
    </li>
  );
}
function ImgProfil({ img }: { img: CoverImageI }) {
  return (
    <div className="shrink-0 h-20 w-20 rounded-full overflow-hidden">
      <Image data={img.responsiveImage} />
    </div>
  );
  /*   return img ? (
      <img src={img} alt={alt} className="shrink-0 h-20 w-20 rounded-full" />
    ) : (
      <div className="shrink-0 h-20 w-20 rounded-full hero-img bg-gray"></div>
    ); */
}
