import { createContext, useEffect } from "react";
import { Outlet, useLocation } from "react-router-dom";
import { Footer } from "../components/Footer";
import ScrollToTop from "../components/ScrollToTop";
import CustomColor from "../components/Common/CustomColor";
import { useQuery } from "graphql-hooks";
import ReactGA from "react-ga";
import CookieBanner from "../components/CookiesBanner";
export const ThemeContext = createContext<any>(null);

export default function Root() {
  const location = useLocation();
  useEffect(() => {
    ReactGA.pageview(location.pathname + location.search);
  }, [location]);
  const { data, error } = useQuery(COLOR_PRIMARY_QUERY);
  if (error) return "error";
  return (
    <div className={`light min-h-screen flex flex-col`}>
      <CookieBanner />
      <CustomColor
        r={data?.layout.accentColor.red || 133}
        g={data?.layout.accentColor.green || 15}
        b={data?.layout.accentColor.blue || 15}
      />
      <ScrollToTop>
        <div id="detail" className="flex-1">
          <Outlet />
        </div>
      </ScrollToTop>
      <Footer />
    </div>
  );
}
const COLOR_PRIMARY_QUERY = `
  query GetPrimaryColor {
    layout {
      accentColor {
        blue
        green
        red
      }
    }
  }
`;
