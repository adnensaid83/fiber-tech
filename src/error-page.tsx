import { useRouteError } from "react-router-dom";

export default function ErrorPage() {
  const error = useRouteError() as any;
  return (
    <div className="min-h-screen flex flex-col items-center justify-center">
      <h1 className="text-3xl">Oops!</h1>
      <p className="text-l">{error.data || error.message}</p>
    </div>
  );
}
