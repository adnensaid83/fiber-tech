import { useState } from "react";

const usePagination = (itemsPerPage: number, initialPage = 1) => {
  const [currentPage, setCurrentPage] = useState(initialPage);

  const goToPage = (pageNumber: number) => {
    setCurrentPage(pageNumber);
  };

  const paginate = (array: any) => {
    const startIndex = (currentPage - 1) * itemsPerPage;
    const endIndex = startIndex + itemsPerPage;
    return array.slice(startIndex, endIndex);
  };

  return {
    currentPage,
    goToPage,
    paginate,
  };
};

export default usePagination;
