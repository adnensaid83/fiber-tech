export interface SocialI {
  id: string;
  title: string;
  url: string;
}
